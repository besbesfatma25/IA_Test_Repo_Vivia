**Project Documentation**

**Overview**

This repository contains the code for a project focused on predicting stress levels based on facial landmarks and human health features. The project provides two options for obtaining facial landmarks and calculating features: one for real-time streaming data and another for processing pre-recorded videos. Additionally, it includes a feature importance calculation and weights updating mechanism using the calculated feature importance.
The implemented solution involves a Flask backend and a Streamlit frontend, creating a simple and interactive application. Users can input human health features, stream a video, and observe stress level predictions along with real-time graphs.

**Components**
1. **Facial Landmarks and Feature Calculation**
The project leverages the MediaPipe library for face detection and facial landmarks extraction. Two options are provided for obtaining facial landmarks and calculating features:
- Streaming Option: Real-time facial landmarks and feature calculation using a streaming webcam feed.
- Video Option: Processing pre-recorded videos to extract facial landmarks and calculate features.
2. **Feature Importance Calculation and Weights Updating**
The system incorporates a mechanism for calculating feature importance and updating weights based on this importance. This dynamic approach aims to enhance the model's predictive capabilities by giving more weight to influential features.
 3.** Frontend**
The frontend is implemented using Streamlit, offering a user-friendly interface for inputting human health features and streaming video. Users can visualize stress predictions along with real-time graphs depicting feature trends.
4. **Flask Backend**
The Flask backend serves as the engine for processing requests, handling video streams, and making stress level predictions. It interfaces with the trained machine learning model and communicates with the frontend.

**Usage**

To run the application locally, follow these steps:

1. Clone the repository: `git clone https://github.com/fatma-id/IA_Test_Repo_Vivia.git’

2. Install dependencies: `pip install -r requirements.txt`
3. Run the Flask backend: `python app.py`
4. Open a new terminal and run the Streamlit frontend: `streamlit run frontend.py`

Navigate to `http://localhost:8501` in your web browser to access the application.

**Future Improvements**

Future improvements could include:

- Enhancing the feature importance calculation algorithm.
- Improving the model training process for stress level prediction.
- Extending the application to include additional health-related features.


